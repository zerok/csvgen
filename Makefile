COMMAND := csvgen

all: bin/$(COMMAND)

bin:
	mkdir -p bin

bin/$(COMMAND): $(shell find . -name '*.go') go.sum go.mod bin
	go build -o $@ ./cmd/$(COMMAND)

clean:
	find . -name 'generated.go' -delete
	rm -rf bin

test:
	find . -name 'generated.go' -delete
	go test ./... -v -test.v -cover

.PHONY: test clean all
