package encoding

import (
	"io"

	"golang.org/x/text/encoding/charmap"
)

func WrapReaderForEncoding(r io.Reader, enc string) io.Reader {
	switch enc {
	case "Windows1252":
		return charmap.Windows1252.NewDecoder().Reader(r)
	default:
		return r
	}
}
