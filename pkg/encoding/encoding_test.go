package encoding

import (
	"bytes"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestWrapReaderForEncoding(t *testing.T) {
	t.Run("no-encoding", func(t *testing.T) {
		b := bytes.Buffer{}
		b.WriteString("hello world")
		r := WrapReaderForEncoding(&b, "")
		output, err := ioutil.ReadAll(r)
		require.NoError(t, err)
		require.Equal(t, "hello world", string(output))
	})
}
