package main

import (
	"context"
	"io/ioutil"
	"os"

	"github.com/rs/zerolog"
	"github.com/spf13/pflag"
	"gitlab.com/zerok/csvgen/internal/generator"
)

func main() {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr})
	ctx := logger.WithContext(context.Background())
	var outputFile string
	var configFilePath string
	pflag.StringVar(&outputFile, "output", "generated.go", "Output file path")
	pflag.StringVar(&configFilePath, "config", "", "Path to a configuration file")
	pflag.Parse()

	if configFilePath == "" {
		configFilePath = "csvgen.yml"
	}
	cfile, err := generator.ParseConfigFile(ctx, configFilePath)
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to load configuration file")
	}

	g := generator.New(cfile)
	output, err := g.Generate(ctx)
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to format code")
	}
	if err := ioutil.WriteFile(outputFile, output, 0644); err != nil {
		logger.Fatal().Err(err).Msg("Failed to write output file")
	}
}
