package generator_test

import (
	"context"
	"io/ioutil"
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/csvgen/internal/generator"
)

func TestGenerate(t *testing.T) {
	t.Run("single", func(t *testing.T) {
		cfg := &generator.Configuration{}
		cfg.PackageName = "main"
		cfg.Processors = []generator.ProcessorConfiguration{
			{
				RecordType: "Record",
				Type:       "Reader",
				Comma:      ",",
			},
		}
		g := generator.New(cfg)
		output, err := g.Generate(context.Background())
		require.NoError(t, err)
		require.NotNil(t, output)
		file := parseFile(t, output)
		requireFunc(t, file, "", "NewReader")
		requireFunc(t, file, "Reader", "parseRecord")
		requireFunc(t, file, "Reader", "Read")
		requireFunc(t, file, "Reader", "ReadAll")
	})

	t.Run("multiple", func(t *testing.T) {
		cfg := &generator.Configuration{}
		cfg.PackageName = "main"
		cfg.Processors = []generator.ProcessorConfiguration{
			{
				RecordType: "Record1",
				Type:       "Reader1",
				Comma:      ",",
			},
			{
				RecordType: "Record2",
				Type:       "Reader2",
				Comma:      ",",
			},
		}
		g := generator.New(cfg)
		output, err := g.Generate(context.Background())
		require.NoError(t, err)
		require.NotNil(t, output)
		file := parseFile(t, output)
		requireFunc(t, file, "", "NewReader1")
		requireFunc(t, file, "", "NewReader2")
		requireFunc(t, file, "Reader1", "parseRecord")
		requireFunc(t, file, "Reader1", "Read")
		requireFunc(t, file, "Reader1", "ReadAll")
		requireFunc(t, file, "Reader2", "parseRecord")
		requireFunc(t, file, "Reader2", "Read")
		requireFunc(t, file, "Reader2", "ReadAll")
	})

	t.Run("with-local-decoder", func(t *testing.T) {
		cfg := &generator.Configuration{}
		cfg.PackageName = "main"
		cfg.Processors = []generator.ProcessorConfiguration{
			{
				RecordType: "Record",
				Type:       "Processor",
				Comma:      ",",
				Fields: map[string]generator.FieldConfiguration{
					"Name": {
						Name:    "Name",
						Column:  0,
						Decoder: generator.FieldDecoderConfiguration{},
					},
					"Date": {
						Name:   "Date",
						Column: 1,
						Decoder: generator.FieldDecoderConfiguration{
							Name: ".parseDate",
						},
					},
				},
			},
		}
		g := generator.New(cfg)
		output, err := g.Generate(context.Background())
		require.NoError(t, err)
		require.NoError(t, ioutil.WriteFile("testcode/with-local-decoder/generated.go", output, 0600))
		testOutput := buildAndRunTestBinary(t, "with-local-decoder")
		require.NotEmpty(t, testOutput)
	})
	t.Run("with-global-decoder", func(t *testing.T) {
		cfg := &generator.Configuration{}
		cfg.PackageName = "main"
		cfg.Processors = []generator.ProcessorConfiguration{
			{
				RecordType: "Record",
				Type:       "Processor",
				Comma:      ",",
				Fields: map[string]generator.FieldConfiguration{
					"Name": {
						Name:    "Name",
						Column:  0,
						Decoder: generator.FieldDecoderConfiguration{},
					},
					"Date": {
						Name:   "Date",
						Column: 1,
						Decoder: generator.FieldDecoderConfiguration{
							Name: "parseDate",
						},
					},
				},
			},
		}
		g := generator.New(cfg)
		output, err := g.Generate(context.Background())
		require.NoError(t, err)
		require.NoError(t, ioutil.WriteFile("testcode/with-global-decoder/generated.go", output, 0600))
		testOutput := buildAndRunTestBinary(t, "with-global-decoder")
		require.NotEmpty(t, testOutput)
	})
}

func buildAndRunTestBinary(t *testing.T, testFolder string) []byte {
	t.Helper()
	cmd := exec.Command("go", "build", "-o", "test")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir = "testcode/" + testFolder
	require.NoError(t, cmd.Run())
	cmd = exec.Command("./test")
	cmd.Dir = "testcode/" + testFolder
	cmd.Stderr = os.Stderr
	output, err := cmd.Output()
	require.NoError(t, err)
	return output
}
