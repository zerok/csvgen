package generator

import (
	"bytes"
	"context"
	"go/format"
	"html/template"
	"strings"
)

const parserTemplate = `package {{.PackageName}}

import "context"
import "encoding/csv"
import "io"
import "gitlab.com/zerok/csvgen/pkg/encoding"

{{ range .Processors }}

func New{{.Type}} (r io.Reader) *{{.Type}} {
        reader := csv.NewReader(encoding.WrapReaderForEncoding(r, "{{ .Encoding }}"))
        reader.Comma = '{{.Comma}}'
        {{ if .FieldsPerRecord -}}
        reader.FieldsPerRecord = {{ .FieldsPerRecord }}
        {{ end -}}
        result := &{{.Type}}{
                reader: reader,
        }
        return result
}

func (r *{{.Type}}) Read(ctx context.Context) ({{.RecordType}}, error) {
        result := {{.RecordType}}{}
        record, err := r.reader.Read()
        if err != nil {
                return result, err
        }
        return r.parseRecord(ctx, record)
}

func (r *{{.Type}}) ReadAll(ctx context.Context) ([]{{.RecordType}}, error) {
        result := make([]{{.RecordType}}, 0, 10)
        for {
                rec, err := r.Read(ctx)
                if err != nil {
                        if err == io.EOF {
                                break
                        }
                        return result, err
                }
                result = append(result, rec)
        }
        return result, nil
}

func (r *{{.Type}}) parseRecord(ctx context.Context, record []string) ({{.RecordType}}, error) {
	var err error
	result := {{.RecordType}}{}
	{{ range .Fields }}
	{{ if .Decoder.Name }}
	result.{{.Name}}, err = {{ if (hasPrefix .Decoder.Name ".") }}r{{ end }}{{.Decoder.Name}}(
			ctx,
			record[{{.Column}}],
			{{ range .Decoder.Options -}}
			"{{ . }}",
			{{ end -}}
	)
	if err != nil {
		return result, err
	}
	{{ else }}
	result.{{.Name}} = record[{{.Column}}]
	{{ end }}
	{{ end }}
	return result, nil
}

{{ end }}
`

type Generator struct {
	cfg *Configuration
}

func New(cfg *Configuration) *Generator {
	return &Generator{cfg: cfg}
}

func (g *Generator) Generate(ctx context.Context) ([]byte, error) {
	funcs := map[string]interface{}{
		"hasPrefix": func(value, prefix string) bool {
			return strings.HasPrefix(value, prefix)
		},
	}
	tmpl := template.Must(template.New("root").Funcs(funcs).Parse(parserTemplate))
	var buffer bytes.Buffer
	if err := tmpl.Execute(&buffer, g.cfg); err != nil {
		return nil, err
	}
	generated := buffer.String()
	output, err := format.Source([]byte(generated))
	return output, err
}
