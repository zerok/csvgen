package generator_test

import (
	"bytes"
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/csvgen/internal/generator"
)

const validConfig = `
packageName: "package"
processors:
- recordType: Record
  type: Reader
  fields:
    FieldName:
      column: 2
`

func TestParseConfig(t *testing.T) {
	t.Run("valid-config", func(t *testing.T) {
		b := bytes.NewBufferString(validConfig)
		out, err := generator.ParseConfig(context.Background(), b)
		require.NoError(t, err)
		require.NotNil(t, out)
		require.Len(t, out.Processors, 1)
		p := out.Processors[0]
		require.Len(t, p.Fields, 1)
		require.Contains(t, p.Fields, "FieldName")
		f := p.Fields["FieldName"]

		// Ensure that the fieldname is properly copied into the field
		require.Equal(t, "FieldName", f.Name)
	})
}
