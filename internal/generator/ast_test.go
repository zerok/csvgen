package generator_test

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"testing"

	"github.com/stretchr/testify/require"
)

func requireFunc(t *testing.T, file *ast.File, recvType string, name string) bool {
	t.Helper()
	found := false
	ast.Inspect(file, func(n ast.Node) bool {
		if found {
			return false
		}
		switch node := n.(type) {
		case *ast.FuncDecl:
			if node.Name.Name != name {
				return false
			}
			if getFuncReceiverType(node) == recvType {
				found = true
			}
			return false
		}
		return true
	})
	if !found {
		t.Fatalf("%s#%s not found in output", recvType, name)
	}
	return found

}

func parseFile(t *testing.T, input []byte) *ast.File {
	t.Helper()
	fset := token.NewFileSet()
	file, err := parser.ParseFile(fset, "generated.go", input, parser.ParseComments)
	require.NoError(t, err)
	require.NotNil(t, file)
	return file
}

func getFuncReceiverType(node *ast.FuncDecl) string {
	if node.Recv == nil {
		return ""
	}
	for _, f := range node.Recv.List {
		switch e := f.Type.(type) {
		case *ast.StarExpr:
			return fmt.Sprintf("%s", e.X)
		case *ast.Ident:
			return e.Name
		}
		return ""
	}
	return ""
}
