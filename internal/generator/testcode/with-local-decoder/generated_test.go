package main

// This file only exists to make global testing still work in a clean checkout.
// It will eventually most likely be replaced with some build flags.

import (
	"context"
	"io"
)

func NewProcessor(r io.Reader) *Processor {
	return &Processor{}
}

func (p *Processor) ReadAll(ctx context.Context) ([]Record, error) {
	return nil, nil
}
