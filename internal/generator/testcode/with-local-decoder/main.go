package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"
	"time"
)

type Record struct {
	Name string
	Date time.Time
}

type Processor struct {
	reader *csv.Reader
}

func (p *Processor) parseDate(ctx context.Context, raw string) (time.Time, error) {
	return time.Parse("02.01.2006", raw)
}

func main() {
	fp, err := os.Open("test.csv")
	if err != nil {
		panic(err)
	}
	defer fp.Close()
	proc := NewProcessor(fp)
	records, err := proc.ReadAll(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Println(records)
}
