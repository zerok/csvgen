package generator

import (
	"context"
	"io"
	"os"

	"gopkg.in/yaml.v3"
)

type ProcessorConfiguration struct {
	RecordType      string                        `yaml:"recordType"`
	Type            string                        `yaml:"type"`
	Fields          map[string]FieldConfiguration `yaml:"fields"`
	FieldsPerRecord *int                          `yaml:"fieldsPerRecord"`
	Encoding        string                        `yaml:"encoding"`
	Comma           string                        `yaml:"comma"`
}

type FieldDecoderConfiguration struct {
	Name    string   `yaml:"name"`
	Options []string `yaml:"options"`
}

type FieldConfiguration struct {
	Name    string                    `yaml:"name"`
	Column  int64                     `yaml:"column"`
	Decoder FieldDecoderConfiguration `yaml:"decoder"`
}

type Configuration struct {
	PackageName string                   `yaml:"packageName"`
	Processors  []ProcessorConfiguration `yaml:"processors"`
}

func ParseConfig(ctx context.Context, r io.Reader) (*Configuration, error) {
	cfile := Configuration{}
	if err := yaml.NewDecoder(r).Decode(&cfile); err != nil {
		return nil, err
	}
	for _, p := range cfile.Processors {
		for name, f := range p.Fields {
			f.Name = name
			p.Fields[name] = f
		}
	}
	if cfile.PackageName == "" {
		cfile.PackageName = os.Getenv("GOPACKAGE")
	}
	return &cfile, nil
}

func ParseConfigFile(ctx context.Context, fpath string) (*Configuration, error) {
	fp, err := os.Open(fpath)
	if err != nil {
		return nil, err
	}
	defer fp.Close()
	return ParseConfig(ctx, fp)
}
